#include<stdio.h>
#include<math.h>
int main(void)
{
	double x[20];
	double fx[20];
	double h[20];
	double a[20];
	double b[20];
	double c[20];
	double p[20][20];
	double M[20];
	double N[20];
	double y0,y1,y,fy,m;
	int i,j;
	int repeat,ri;
	printf("输入端点处导数值");
	scanf("%lf%lf",&y0,&y1);
	printf("输入函数表");
	for(i=0;i<=18;i++){
		scanf("%lf%lf",&x[i],&fx[i]);
	}
	for(i=1;i<=18;i++){
		h[i]=x[i]-x[i-1];
	}
	for(i=1;i<=17;i++){
		a[i]=h[i]/(h[i]+h[i+1]);
		b[i]=h[i+1]/(h[i]+h[i+1]);
		c[i]=6.0*((fx[i+1]-fx[i])/h[i+1]-(fx[i]-fx[i-1])/h[i])/(h[i]+h[i+1]);
	}
	c[0]=6.0/h[1]*((fx[1]-fx[0])/h[1]-y0);
	c[18]=6.0/h[18]*(y1-(fx[18]-fx[17])/h[18]);
	a[18]=1;
	b[0]=1;
	for(i=1;i<=19;i++){
		for(j=1;j<=19;j++){
			if(i==j){
				p[i][j]=2;
			}
			else if(i==j+1){
				p[i][j]=a[j];
			}
			else if(i==j-1){
				p[i][j]=b[i-1];
			}
			else{
				p[i][j]=0;
			}
		}
	}
	for(i=2;i<=19;i++){
		m=p[i][i-1]/p[i-1][i-1];
		c[i-1]=c[i-1]-c[i-2]*m;
		for(j=i;j<=19;j++){
			p[i][j]=p[i][j]-p[i-1][j]*m;
		}
	}
	M[18]=c[18]/p[19][19];
	for(i=18;i>0;i--){
		m=0;
		for(j=i+1;j<=19;j++){
			m=m+p[i][j]*M[j-1];
		}
		M[i-1]=(c[i-1]-m)/p[i][i];
	}
	printf("计算次数");
	scanf("%d",&repeat);
	for(ri=1;ri<=repeat;ri++){
		printf("输入自变量的值"); 
		scanf("%lf",&y);
		for(i=1;y>x[i];i++){
		} 
		fy=M[i-1]*pow((x[i]-y),3)/(6*h[i])+M[i]*pow((y-x[i-1]),3)/(6*h[i])+(fx[i-1]-M[i-1]*h[i]*h[i]/6)*(x[i]-y)/h[i]+(fx[i]-M[i]*h[i]*h[i]/6)*(y-x[i-1])/h[i];
		printf("f(%.0f)=%.5f\n",y,fy);
	}
}
	

