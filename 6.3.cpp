#include<stdio.h>
int main(void)
{
	double x[10];
	double y[10];
	double K1,K2,K3,K4;
	int i; 
	double f(double x,double y);
	x[0]=0;
	y[0]=1;
	for(i=0;i<=9;i++){
		x[i+1]=0.1*(i+1);
		K1=f(x[i],y[i]);
		K2=f(x[i]+0.05,y[i]+0.05*K1);
		K3=f(x[i]+0.05,y[i]+0.05*K2);
		K4=f(x[i]+0.1,y[i]+0.1*K3);
		y[i+1]=y[i]+0.1*(K1+2*K2+2*K3+K4)/6;
		printf("y(%.1f)=%f\n",x[i+1],y[i+1]);
	}
} 
double f(double x,double y)
{
	double r;
	r=2.0*x/(3*y*y);
	return r;
}
