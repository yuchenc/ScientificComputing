#include<stdio.h>
#include<math.h>
int main(void)
{
	double y[20];
	double t[20];
	double a,b,r,S,M,f,g,fa,fb,ga,gb;
	int i;
	int ri,repeat;
	printf("输入函数表的函数值");
	for(i=0;i<=19;i++){
		scanf("%lf",&y[i]);
	}
	printf("输入牛顿迭代法的迭代次数");
	scanf("%d",&repeat);
	printf("输入a和b的初始值");
	scanf("%lf%lf",&a,&b); 
	for(ri=1;ri<=repeat;ri++){
		S=0;
		M=0;
		f=0;
		g=0;
		fa=0;
		fb=0;
		ga=0;
		gb=0;
		for(i=1;i<=19;i++){
			f=f+2*(a*exp(i*b)-y[i])*exp(i*b);
			g=g+2*(a*exp(i*b)-y[i])*i*a*exp(i*b);
			fa=fa+2*exp(2*i*b);
			fb=fb+2*(2*i*a*exp(i*b)-i*y[i])*exp(i*b);
			gb=gb+2*(2*i*i*a*exp(i*b)-i*i*y[i])*a*exp(i*b);
		} 
		ga=fb;
		r=fa*gb-fb*ga;
		a=a-(f*gb-g*fb)/r;
		b=b-(fa*g-ga*f)/r;
		for(i=1;i<=19;i++){
			t[i]=a*exp(i*b)-y[i];
		}
		for(i=1;i<=19;i++){
			S=S+pow(t[i],2);
			if(t[i]<0){
				t[i]=-t[i];
			} 
			if(t[i]>M){
				M=t[i];
			} 
		}
		S=sqrt(S);
		printf("a=%f b=%f S=%f M=%f\n",a,b,S,M);
	}
} 
